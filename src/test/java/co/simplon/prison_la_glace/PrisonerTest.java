package co.simplon.prison_la_glace;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import co.simplon.prison_la_glace.entity.Prisoner;
import co.simplon.prison_la_glace.repository.PrisonerDAO;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PrisonerTest {

    private static final String PIERRE_FIRSTNAME = "Pierre";
    private static final String PIERRE_LASTNAME = "Martinet";
    private static final String PAUL_FIRSTNAME = "Paul";
    private static final String PAUL_LASTNAME = "Bocuse";

    @Autowired
    private PrisonerDAO prisonerDAO;

    private Prisoner pierre;

    @BeforeEach
    void init() {
        pierre = new Prisoner();
        pierre.setFirstName(PIERRE_FIRSTNAME);
        pierre.setLastName(PIERRE_LASTNAME);
    }

    @Test
    public void testAddNewPrisoner() {
        assertTrue(prisonerDAO.create(pierre));
        assertNotNull(pierre.getId());
        assertEquals("Pierre", pierre.getFirstName());
        assertEquals("Martinet", pierre.getLastName());
    }

    @Test
    public void testFindByIdPrisoner() {
        prisonerDAO.create(pierre);
        assertNotNull(prisonerDAO.findById(pierre.getId()));
    }

    @Test
	public void testFindById() {
		Prisoner prisoner = prisonerDAO.findById(77);
		System.out.println(
            "*********************\n\n" + 
            "@TEST : FIND BY ID\n\n" + 
            prisoner + 
            "\n\n*********************"
        );
	}


    @Test
    public void testFindAllPrisoner() {
        assertNotNull(prisonerDAO.findAll());
    }

    @Test
    public void testUpdateById() {
        prisonerDAO.create(pierre);
        assertNotNull(pierre.getId());

        pierre.setFirstName(PAUL_FIRSTNAME);
        pierre.setLastName(PAUL_LASTNAME);
        
        assertEquals("Paul", pierre.getFirstName());
        assertEquals("Bocuse", pierre.getLastName());
        
        assertTrue(prisonerDAO.update(pierre));
    }

    @Test
    public void testDeletePrisonerById() {
        prisonerDAO.create(pierre);
        assertTrue(prisonerDAO.delete(pierre.getId()));
    }

}