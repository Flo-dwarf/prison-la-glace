package co.simplon.prison_la_glace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import co.simplon.prison_la_glace.repository.IncidentDAO;

@SpringBootTest
public class IncidentTest {

    private static final String PIERRE_FIRSTNAME = "Pierre";
    private static final String PIERRE_LASTNAME = "Martinet";
    private static final String PAUL_FIRSTNAME = "Paul";
    private static final String PAUL_LASTNAME = "Bocuse";

    @Autowired
    private IncidentDAO incidentDAO;

}