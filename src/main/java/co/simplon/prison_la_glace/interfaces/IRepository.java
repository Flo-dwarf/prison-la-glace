package co.simplon.prison_la_glace.interfaces;

import java.util.List;

public interface IRepository<T> {

    public List<T> findAll();

    public boolean create(T entity);

    public T findById(int id);

    public boolean update(T entity);

    public boolean delete(int id);
}
