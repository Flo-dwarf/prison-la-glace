package co.simplon.prison_la_glace.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.prison_la_glace.entity.Prisoner;

@Repository
public class PrisonerDAO extends CrudRepository<Prisoner> {

    @Autowired
    private DataSource dataSource;
    private Connection connection;

    public PrisonerDAO() {
        tableName = "prisoners";
        tableEntityId = "prisonerId";
        tableArgument = "firstName = ?, lastName = ? WHERE prisonerId = ?";
        entityName = new Prisoner();
    }

    @Override
    protected Prisoner resultSetToEntity(ResultSet result) throws SQLException {
        entityName = new Prisoner(result.getInt("prisonerId"),
        result.getString("firstName"),
        result.getString("lastName"));
        return entityName;
    }

    @Override
    protected void stmtToEntity(Prisoner prisoner, PreparedStatement stmt) throws SQLException {
        stmt.setString(1, prisoner.getFirstName());
        stmt.setString(2, prisoner.getLastName());
        stmt.setInt(3, prisoner.getId());
    }






    @Override
    public boolean create(Prisoner prisoner) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("ADD_PRISONER"), PreparedStatement.RETURN_GENERATED_KEYS);
            ;
            stmt.setString(1, prisoner.getFirstName());
            stmt.setString(2, prisoner.getLastName());
            stmt.executeUpdate();

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                prisoner.setId(result.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    // @Override
    // public boolean update(Prisoner prisoner) {
    //     try {
    //         PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
    //                 .prepareStatement(env.getProperty("UPDATE_PRISONER"));
    //         stmt.setString(1, prisoner.getFirstName());
    //         stmt.setString(2, prisoner.getLastName());
    //         stmt.setInt(3, prisoner.getId());
    //         stmt.executeUpdate();
    //         return stmt.executeUpdate() == 1;
    //     } catch (Exception e) {
    //         e.printStackTrace();
    //     } finally {
    //         DataSourceUtils.releaseConnection(connection, dataSource);
    //     }
    //     return false;
    // }

    @Override
    public boolean delete(int id) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("DELETE_PRISONER_BY_ID"));
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    public List<Prisoner> getPrisonersByIncidents() {
        List<Prisoner> list = new ArrayList<>();
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("GET_PRISONERS_BY_INCIDENTS"));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(new Prisoner(
                        rs.getInt("prisonerId"),
                        rs.getString("firstName"),
                        rs.getString("lastName")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }


}
