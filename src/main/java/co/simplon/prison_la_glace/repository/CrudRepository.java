package co.simplon.prison_la_glace.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.prison_la_glace.interfaces.IRepository;

@Repository
public abstract class CrudRepository<T> implements IRepository<T> {

    @Autowired
    private DataSource dataSource;
    private Connection connection;

    @Autowired
    Environment env;

    protected String tableName;
    protected String tableArgument;
    protected String tableEntityId;
    protected T entityName;

    protected abstract T resultSetToEntity(ResultSet result) throws SQLException;

    protected abstract void stmtToEntity(T entity, PreparedStatement stmt) throws SQLException;

    protected Connection connect() throws SQLException {
        Connection cnx = DataSourceUtils.getConnection(dataSource);
        return cnx;
    }

    protected String requestSQL(String request) throws SQLException {
        return env.getProperty(request).replace("tableNameToChange", tableName);
    }

    @Override
    public List<T> findAll() {
        List<T> list = new ArrayList<>();
        try {
            PreparedStatement stmt = connect().prepareStatement(requestSQL("GET"));
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(resultSetToEntity(result));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    @Override
    public T findById(int id) {
        try {
            PreparedStatement stmt = connect()
                    .prepareStatement(requestSQL("GET_ID").replace("tableEntityId", tableEntityId));
            ResultSet result = stmt.executeQuery();
            stmt.setInt(1, id);

            if (result.next()) {
                return resultSetToEntity(result);
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return entityName;
    }

    @Override
    public boolean update(T entity) {
        try {
            PreparedStatement stmt = connect()
                    .prepareStatement(requestSQL("UPDATE_PRISONER").replace("tableArgument", tableArgument));
            stmtToEntity(entity, stmt);
            stmt.executeUpdate();
            return stmt.executeUpdate() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    @Override
    public boolean create(T entity) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean delete(int id) {
        // TODO Auto-generated method stub
        return false;
    }
}
