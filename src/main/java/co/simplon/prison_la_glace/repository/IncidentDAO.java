package co.simplon.prison_la_glace.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.prison_la_glace.entity.Incident;
import co.simplon.prison_la_glace.interfaces.IRepository;

@Repository
public class IncidentDAO implements IRepository<Incident> {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private Environment env;

    private Connection connection;

    @Override
    public List<Incident> findAll() {
        List<Incident> list = new ArrayList<>();
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("GET_INCIDENTS"));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(new Incident(
                        rs.getInt("incidentId"),
                        rs.getString("arrestingTown"),
                        rs.getString("incidentDescription")));
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    @Override
    public boolean create(Incident incident) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("ADD_INCIDENT"));
            stmt.setString(1, incident.getArrestingTown());
            stmt.setString(2, incident.getIncidentDescription());
            stmt.executeUpdate();
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                incident.setId(result.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    @Override
    public Incident findById(int id) {
        Incident incident = null;

        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("GET_INCIDENT_BY_ID"));
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                return new Incident(
                        rs.getInt("incidentId"),
                        rs.getString("arrestingTown"),
                        rs.getString("incidentDescription"));
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return incident;
    }

    @Override
    public boolean update(Incident incident) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("UPDATE_INCIDENT_BY_ID"));
            stmt.setString(1, incident.getArrestingTown());
            stmt.setString(2, incident.getIncidentDescription());
            stmt.setInt(3, incident.getId());
            return stmt.executeUpdate() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("DELETE_INCIDENT_BY_ID"));
            return stmt.executeUpdate() == 1;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    public List<Incident> getIncidentsByPrisoners() {
        List<Incident> list = new ArrayList<>();
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement(env.getProperty("GET_INCIDENTS_BY_PRISONERS"));
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                list.add(new Incident(
                        rs.getInt("prisonerId"),
                        rs.getString("firstName"),
                        rs.getString("lastName")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

}
