package co.simplon.prison_la_glace.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.prison_la_glace.repository.PrisonerDAO;

@Controller
public class PrisonerController {

    @Autowired
    PrisonerDAO prisonerDAO;

    @GetMapping
    public String getAllPrisoner(Model model) {
        model.addAttribute("prisoner", prisonerDAO.findAll());
        return "index";
    }
}
