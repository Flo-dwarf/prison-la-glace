package co.simplon.prison_la_glace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrisonLaGlaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrisonLaGlaceApplication.class, args);
	}

}
