package co.simplon.prison_la_glace.entity;

import java.util.ArrayList;
import java.util.List;

public class Incident {
    private Integer incidentId;
    private String arrestingTown;
    private String incidentDescription;
    private List<Prisoner> prisoners = new ArrayList<>();

    public Incident() {
    }

    public List<Prisoner> getPrisoners() {
        return prisoners;
    }

    public void setPrisoners(List<Prisoner> prisoners) {
        this.prisoners = prisoners;
    }

    public Incident(String arrestingTown, String incidentDescription) {
        this.arrestingTown = arrestingTown;
        this.incidentDescription = incidentDescription;
    }

    public Incident(Integer incidentId, String arrestingTown, String incidentDescription) {
        this.incidentId = incidentId;
        this.arrestingTown = arrestingTown;
        this.incidentDescription = incidentDescription;
    }

    public Integer getId() {
        return incidentId;
    }

    public void setId(Integer incidentId) {
        this.incidentId = incidentId;
    }

    public String getArrestingTown() {
        return arrestingTown;
    }

    public void setArrestingTown(String arrestingTown) {
        this.arrestingTown = arrestingTown;
    }

    public String getIncidentDescription() {
        return incidentDescription;
    }

    public void setIncidentDescription(String incidentDescription) {
        this.incidentDescription = incidentDescription;
    }
}
