package co.simplon.prison_la_glace.entity;

import java.util.ArrayList;
import java.util.List;

public class Prisoner {
    private Integer prisonerId;
    private String firstName;
    private String LastName;
    private List<Incident> incidents = new ArrayList<>();

    public Prisoner() {
    }

    public List<Incident> getIncidents() {
        return incidents;
    }

    public void setIncidents(List<Incident> incidents) {
        this.incidents = incidents;
    }

    public Prisoner(String firstName, String lastName) {
        this.firstName = firstName;
        LastName = lastName;
    }

    public Prisoner(Integer prisonerId, String firstName, String lastName) {
        this.prisonerId = prisonerId;
        this.firstName = firstName;
        LastName = lastName;
    }

    public Integer getId() {
        return prisonerId;
    }

    public void setId(Integer prisonerId) {
        this.prisonerId = prisonerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

}
