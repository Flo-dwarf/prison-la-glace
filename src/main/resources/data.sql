-- INSERT DATA PRISONER
INSERT INTO
  `prisoners`(`firstName`, `lastName`)
VALUES
  ("Pierre", "Martinet");
-- INSERT DATA INCIDENT
INSERT INTO
  `incidents`(`arrestingTown`, `incidentDescription`)
VALUES
  ("Martinet", "Le traiteur intraitable !");
-- INSERT DATA Prisonnier / INCIDENT
INSERT INTO
  `prisoners_incidents`(`prisonerId`, `incidentId`)
VALUES
  (1, 1);