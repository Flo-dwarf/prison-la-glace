-- CREATE DATABASE
CREATE DATABASE IF NOT EXISTS `prison_la_glace`;
USE `prison_la_glace`;
-- CREATE TABLE PRISONER
DROP TABLE IF EXISTS `prisoners`;
CREATE TABLE prisoners (
  prisonerId INTEGER PRIMARY KEY AUTO_INCREMENT,
  firstName VARCHAR(255) NOT NULL,
  lastName VARCHAR(255) NOT NULL
);
-- CREATE TABLE INCIDENT
DROP TABLE IF EXISTS `incidents`;
CREATE TABLE incidents (
  incidentId INTEGER PRIMARY KEY AUTO_INCREMENT,
  arrestingTown VARCHAR(255) NOT NULL,
  incidentDescription TEXT NOT NULL
);
-- CREATE TABLE JOINTE PRISONNIER / INCIDENT
DROP TABLE IF EXISTS `prisoners_incidents`;
CREATE TABLE `prisoners_incidents` (
  `prisonerId` int(11) NOT NULL,
  `incidentId` int(11) NOT NULL,
  FOREIGN KEY (prisonerId) REFERENCES prisoners (prisonerId),
  FOREIGN KEY (incidentId) REFERENCES incidents (incidentId)
);